# Dirk Dittmer 20190311 ----

# Set working directory to source file location
# Each coverage table file has to be a .csv file
# Each coverage table file has to have the same length
# Each coverage table file has to live in folder "TheData"
# Folder "TheData" has to be in the same directory as the source code

# libraries ----
library(gdata)
library(reshape2)
library(MASS)
library(RColorBrewer)
library(FactoMineR)
library(heatmap.plus)
library(DMwR)
library(Hmisc)
library(vegan)
library(TTR)		# provides function "runSum"
library(zoo)
library(data.table)


# Global settings ----
options(digits=3)
options(stringsAsFactors = FALSE)


# Functions ----

# read in multiple spreadsheets each with primers and one or more experiments
# the experiments are in  columns
readBigDataTable <- function(x){
	filnamelistlength <- length(x)
	 
	mir <- fread(x[1])
	mir <- na.omit(mir)
	mir <- drop.levels(mir)
	mir$filename <- factor(x[1])
	ReadInData <- mir
	print(nrow(ReadInData))
	
	if (filnamelistlength< 2) return (ReadInData)
	
	for (i in 2:filnamelistlength)
	{
	mir <- fread(x[i])
	mir <- na.omit(mir)
	mir <- drop.levels(mir)
	mir$filename <- factor(x[i])
	
	ReadInData <- rbind(ReadInData,mir)
	print(nrow(ReadInData))
	}	
	return (ReadInData)
}
anscombe <- function(x){
	2*sqrt(x + 3/8)
}
cube <- function(x){
	(x+1)^(1/3)
}
IQR.adjusted <- function(x){
	IQR(x)/1.349
}
plot3Coverages <- function(Try2m) {
  par(mfrow = c (3,1))
  par(cex=1.5)
  par(bty="n")
  par(new = FALSE)
  
  #panel A
  plot(Try2m[,1], 
       main = "009",
       col = "darkred", 
       lwd = 2, 
       type ="h", 
       las = 1, 
       ylab = expression(sqrt("coverage",3)), 
       xlab = "relative position", 
       ylim = c(0,2),
       xaxs = "i")
  
  
  #panel B
  plot(Try2m[,2], 
       main = "010",
       col = "darkred", 
       lwd = 2, 
       type ="h", 
       las = 1, 
       ylab = expression(sqrt("coverage",3)),
       xlab = "relative position", 
       ylim = c(0,2),
       xaxs = "i")
  
  #panel C
  plot(Try2m[,3], 
       main = "011",
       col = "darkred", 
       lwd = 2, 
       type ="h", 
       las = 1, 
       ylab = expression(sqrt("coverage",3)),
       xlab = "relative position", 
       ylim = c(0,2),
       xaxs = "i")
  return(TRUE)
}
plot2Coverages <- function(Try2m) {
  Try2m$position = row(Try2m)[,1]
  par(mfrow = c (1,1))
  par(cex=2)
  par(bty="o")
  par(new = FALSE)
  
  #panel A
  plot(Try2m$position*33,Try2m[,1], 
       main = "",
       col = "gray", 
       lwd = 2, 
       type ="o", 
       las = 1, 
       ylab = expression(log(sqrt("coverage",3))), 
       xlab = "Position", 
       ylim = c(0,2),
       xaxs = "i",
       pch = 20
  ) #log = "y"
  
  par(new = TRUE)
  plot(Try2m$position*33,Try2m[,2], 
       main = "",
       col = "darkred", 
       lwd = 2, 
       type ="s", 
       las = 1, 
       ylab = "", 
       xlab = "", 
       ylim = c(0,2),
       xaxs = "i"
  ) #log = "y"
  par(new = FALSE)
  return(TRUE)
}
sumOfCoverage <- function(ReadInData) {
  # Only needed if there are many gaps, 
  # will change numbering of the underlying sequence
  # Define sumOfCoverage across all samples
  i = 2
  a = ReadInData[, i, with=FALSE]
  for (i in 3:ncol(ReadInData)) { 
    print(i)
    a = ReadInData[,i,with=FALSE]  + a
  }
  ReadInData$sumOfCoverage = a
  return(ReadInData)
}
# Read in the data ----
# Each coverage table file has to be a .csv file
# Each coverage table file has to have the same length
# Each coverage table file has to live in folder "TheData"
# Folder "TheData" has to be in the same directory as the source code
filenames <- c("KICS009.csv",
               "KICS010.csv",
               "KICS011.csv")
getwd()
setwd("TheData")
ReadInData = readBigDataTable(filenames)
setwd("..")

# summary(ReadInData)
head(ReadInData)
table(ReadInData$filename)


# Save Copy of Combined Dataset ----
#
ReadInDataall <- ReadInData
ReadInData <- ReadInDataall  #  This will always re-create the initial data set
# write.table(ReadInDataall,file = "CompleteDataSet.txt", sep = "\t")


# Data cleaning ----
#
#																				OUT:		ReadInData
head(ReadInData)
colnames(ReadInData)

ReadInData$Position <- as.factor(ReadInData$Position)
head(ReadInData)
head(ReadInData[20])
summary(ReadInData)

print((nrow(ReadInData) * ncol(ReadInData)))

ReadInData <- dcast(data = ReadInData, value.var = "Coverage",
               Position ~ filename, drop = FALSE,
               fun = mean, fill = 0)
head(ReadInData)
nrow(ReadInData)
ncol(ReadInData)
summary(ReadInData)


ReadInData = sumOfCoverage(ReadInData)
ReadInData = drop.levels(ReadInData)
ReadInData = na.omit(ReadInData)


ReadInData$Position = as.numeric(as.character(ReadInData$Position))
ReadInData = as.matrix(ReadInData)
ReadInData = as.data.frame(ReadInData)
ReadInData = ReadInData[,1:ncol(ReadInData)-1]
rownames(ReadInData) = ReadInData$Position
summary(ReadInData)
head(ReadInData)

# Data Transformation ----
# ReadInData has to have the following structure
# ReadInData is a dataframe
# ReadInData is all numeric
# ReadInData [, 1] indicates the position of the genome (need not be continues)
# rownames(ReadInData) indicates the position of the genome (need not be continues)
# colnames (ReadInData) refer to sample file names

# Drop out known bad columns/samples
#
# colnames(ReadInData)[15]
# ReadInData <- ReadInData[,-15]  


# Count Transformation using either cube-root or Anscombe 
# followed by log(Count+1)
head(ReadInData[,2:ncol(ReadInData)])
# ReadInData[,2:ncol(ReadInData)] <- anscombe(ReadInData[,2:ncol(ReadInData)])  # anscombe transform 
ReadInData[,2:ncol(ReadInData)] <- cube(ReadInData[,2:ncol(ReadInData)]) # cube transform 
ReadInData[,2:ncol(ReadInData)] <- log1p(ReadInData[,2:ncol(ReadInData)]) #logtransform


# median centering 
# med.att = apply(ReadInData[,2:ncol(ReadInData)], 2, median)
# print(med.att)
# ReadInData[,2:ncol(ReadInData)] = sweep(ReadInData[,2:ncol(ReadInData)], 2, med.att,"-")

# IQR scaling
# iqr.att = apply(ReadInData[,2:ncol(ReadInData)], 2, IQR.adjusted)
# print(iqr.att)
# ReadInData[,2:ncol(ReadInData)] = sweep(ReadInData[,2:ncol(ReadInData)], 2, iqr.att,"/")

# using default scaling function
# ReadInData[,2:ncol(ReadInData)] = scale(ReadInData[,2:ncol(ReadInData)], center = FALSE, scale = TRUE)


# colMeans(ReadInData)
# setting low coverage to mean coverage = 0
# ReadInData[ReadInData[,2]<0,2] <- 0
# ReadInData[ReadInData[,3]<0,3] <- 0

# Figure: Explore gene coverage ----
plotDensityUsingBaseR <- function(ReadInData) {
  # Densityplot of gene coverage using the baseR function 
  par(mfrow = c (1,1))
  par(cex=2)
  par(bty="o")
  
  par(new = FALSE)
  plot(density(ReadInData[,2]), 
       main = "Coverage", 
       xlim = c(0,2), 
       lwd = 2, 
       type ="l", 
       las = 1, 
       col = "blue", 
       xlab ="", 
       ylab="")
  
  rug(ReadInData[,2], col = "blue")
  
  text (0,2,"09",   col = "blue", adj = c(0,0))
  text (0,1.2,"10", col = "red",  adj = c(0,0))
  text (0,0.6,"11", col = "gray", adj = c(0,0))
  
  par(new = TRUE)
  plot(density(ReadInData[,3]), 
       main = "", 
       xlim = c(0,2), 
       lwd = 2, 
       type ="l", 
       las = 1, 
       col = "red", 
       xaxt = "n", 
       yaxt = "n")
  
  par(new = TRUE)
  plot(density(ReadInData[,4]), 
       main = "", 
       xlim = c(0,2), 
       lwd = 2, 
       type ="l", 
       las = 1, 
       col = "gray", 
       xaxt = "n", 
       yaxt = "n")
  
  par(new = FALSE)
}
plotDensityUsingBaseR(ReadInData)

# Figure: Simple Profile ----
plotFirst2SamplesCoverage <- function(ReadInData) {
  # ReadInData[,2] corresponds to sample 1
  # ReadInData[,1] corresponds to genome position
  summary(ReadInData)
  par(mfrow = c (1,2))
  par(cex=1.6)
  par(bty="o")
  par(new = FALSE)
  # panel A
  colnames(ReadInData)
  plot(ReadInData[,1],                      # genome position
       runMedian(ReadInData[,2],41),        # transformed coverage for sample 1
       main = "",
       col = "darkblue", 
       lwd = 1, 
       type ="h", 
       las = 1, 
       ylab = "", 
       xlab = "", 
       xaxs = "i", 
       xaxt = "n", 
       yaxt = "n", 
       pch = ".",
       ylim = c(-5,5))
  
  par(new = TRUE)
  plot(ReadInData[,1],                   # genome position
       -runMedian(ReadInData[,3],41),    # transformed coverage for sample 2
       main = "Sample 1 and 2",
       col = "darkred", 
       lwd = 1, 
       type ="h", 
       las = 1, 
       ylab = "Scaled Coverage (41bp window)", 
       xlab = "Genome Position", 
       xaxs = "i",
       ylim = c(-5,5))
  
  # panel B
  par(new = FALSE)
  qqnorm(runMedian(ReadInData[,2]),            # transformed coverage for sample 1
  pch = 19,
  cex = 0.5, 
  col = "darkblue",
  main = "41 bp window")
  qqline(runMedian(ReadInData[,2]),
         col = "darkred", 
         lwd = 2, 
         lty = 3, 
         las = 1)
  histSpike(runMedian(ReadInData[,2]), 
            add=TRUE, 
            col = "darkgray", 
            frac = 0.3, 
            lwd = 3, 
            side =2, 
            las = 1)
  return(TRUE)
}
Dummy = plotFirst2SamplesCoverage(ReadInData)


# Simplification ----
# summary(ReadInData)
# write out normalized data to to file
# write.table(ReadInData,file = "normalized.txt", sep = "\t")


# 99-wide window ----
colnames(ReadInData)
ReadInData$PositionAtEnd = ReadInData$Position
ReadInData99 = rollapply(ReadInData[,2:ncol(ReadInData)], 
               width = 99, 
               FUN = max, 
               by = 33)
ReadInData99 = data.frame(ReadInData99)
# head(m2)
# nrow(m2)
# summary(m2)
# write.table(m2,file = "reducedData.txt", sep = "\t")

# Figure: another distribution
OverallDistribution <- function(m2) {
  head(m2)
  m2$position <- factor(rownames(m2))
  m2.melt <- melt(m2[1:(ncol(ReadInData)-1)])
  head(m2.melt)
  
  par(new = FALSE)	
  par(mfrow = c(1,1))
  par(cex = 1.8)
  par(bty = "o")
  qqnorm(m2.melt$value, 
  pch = 19,
  cex = 0.5, 
  col = "darkblue",
  main = "99 bp window, 33 bp overlapp")
  # qqline(m2.melt$value, col = "darkred", lwd = 2, lty = 3)
  histSpike(m2.melt$value, 
            add=TRUE, 
            col = "darkred", 
            frac = 0.5, 
            lwd = 3, side =2)
  return(TRUE)
}
Dummy = OverallDistribution(ReadInData99)

plotHeatMap <- function(m2) {
  head(m2)
  summary(m2)
  nrow(m2)
  Try2m <- as.matrix(m2[,1:ncol(m2)-1])
  rownames(Try2m) <- m2[,ncol(m2)]
  
  # median centering 
  # med.att <- apply(Try2m, 2, median)
  # med.att
  # Try2m <- sweep(Try2m,2,med.att,"-")
  summary(Try2m)
  head(Try2m)
  Try2m <- t(Try2m)
  brewer.palette <- colorRampPalette(rev(brewer.pal(11, "RdYlBu")),
                                     space = "rgb", 
                                     bias = 1.5, 
                                     interpolate = "linear")
  heatmap.plus(Try2m, 
               na.rm = T, 
               scale = "none", 
               col = brewer.palette (55), 
               hclustfun=function(m) hclust(m, method="ward"), 
               distfun = function(x) dist(x, method = "manhattan"), 
               margins = c(5,10), 
               Rowv = NA, 
               Colv = NA, 
               cexCol = 0.1)
  Try2m = t(Try2m)
  Try2m = as.data.frame(Try2m)
  Try2m$median = apply(Try2m, 1, median)
  Try2m$var.att <- apply(Try2m[,1:ncol(Try2m)-1],1,IQR)
  return(Try2m)
}
ReducedData = plotHeatMap(ReadInData99)


summary(ReducedData)
Dummy = plot3Coverages(ReducedData)
Dummy = plot2Coverages(ReducedData)
